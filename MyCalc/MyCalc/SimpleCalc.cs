﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Math;


namespace MyCalc
{
        public class SimpleCalc
    {
        public double Add(double a, double b)
        {
            return a + b;
        }

        public double Sub(double a, double b)
        {
            return a - b;
        }

        public double Mult(double a, double b)
        {
            return a * b;
        }

        public double  Div(double a, double b)
        {
            if (b == 0.0d)
                throw new DivideByZeroException();
            return a / b;
        }
            
    }
}
