﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyCalc
{    
    public class ScienceCalc : SimpleCalc
    {
        public double Sinus(double x)
        {
            return Math.Sin((x / 180D) * Math.PI);
        }
        
        public double Tang(double x)
        {
            return Math.Tan(x);
        }

        public double Cos(double x)
        {
            return Math.Cos(x);
        }

        public double Remind(double x, double y)
        {
            return Math.IEEERemainder(x, y);
        }

        public double MainTrigonom(double x)
        {
            return Math.Pow(Math.Sin(x),2) + Math.Pow(Math.Cos(x),2);            
        }
        
        public int ArrayMin(int[] array)
        {
            return array.Min();
        }

        public int ArrayMax(int[] array)
        {
            return array.Max();
        }

        public dynamic ArraySum(int[] array)
        {
            return array.Sum();
        }
    }
   
      
}
