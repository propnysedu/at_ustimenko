﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MyCalc;


namespace CalcTests
 
{     
    [TestFixture, Order(2)]
    public class SimpleCalcTest
    {
        ScienceCalc auto;

        [SetUp]
        public void Init()
        {
            auto = new ScienceCalc();
        }
        
        [TearDown]
        public void Dispose()
        {
            auto = null;            
        }

        [Test]
        public void uTest_AddPos_1()
        {
            Assert.AreEqual(3.01, auto.Add(2, 1.01));
        }

        [Test]
        public void uTest_AddPos_2()
        {            
            Assert.AreNotEqual(4, auto.Add(2, 1));
        }

        [Test]
        public void uTest_AddNeg_1()
        {
            Assert.AreEqual(-4, auto.Add(-2, -2));
        }

        [Test]
        public void uTest_AddNeg_2()
        {
            Assert.AreNotEqual(4, auto.Add(4, -2));
        }

        [Test]
        public void uTest_AddZero_1()
        {
            Assert.AreEqual(0, auto.Add(-0, 0));            
        }

        [Test]
        public void uTest_AddZero_2()
        {
            Assert.AreNotEqual(0.00000000001, auto.Add(-0, -0));
        }
        
        public void uTest_Mult_1()
        {
            Assert.AreEqual(6, auto.Mult(2, 3));            
        }

        [Test]
        public void uTest_Mult_2()
        {
            Assert.AreEqual(0.06, auto.Mult(0.2, 0.3));
        }

        [Test]
        public void uTest_Mult_3()
        {
            Assert.AreNotEqual(999, auto.Mult(999, 0));
        }
        
        [Test]
        public void uTest_Divis_1()
        {
            Assert.AreEqual(6, auto.Div(12, 2));
        }

        [Test]
        public void uTest_Divis_2()
        {
            Assert.AreNotEqual(2, auto.Div(0, 2));
        }

        [Test]
        public void uTest_Divis_3()
        {
            Assert.AreEqual(0.06, auto.Div(0.12, 2));            
        }
    }
}
