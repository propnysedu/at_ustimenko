﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using MyCalc;


namespace CalcTests
{
    [TestFixture, Order(1)]
    class ScienceCalcTest
    {
        ScienceCalc auto;
        int[] nums = { 3, 6, 8, 59, 63, 15, -22 };
        string EmptyText;
        string NotEmptyText;

        [SetUp]
        public void Init()
        {
            auto = new ScienceCalc();            
        }

        [TearDown]
        public void Dispose()
        {
            auto = null;
        }

        [OneTimeSetUp]
        public void TimeSetup()
        {
            string EmptyText = string.Empty;
            NotEmptyText = "Is not empty";
        }

        [OneTimeTearDown]
        public void TimeTearDown()
        {
            NotEmptyText = "";
        }

        [Test(ExpectedResult = 1), Description("Main triganometric identity")]        
        public double Test_Triganomentric_identity()
        {
            return auto.MainTrigonom(15);           
        }

        [Test(ExpectedResult = 1), Description("Shows sinus in radians")]
        public double Test_Sin_inRads()
        {
            return auto.Sinus(90d);
        }

        [Test, Description("Shows cosinus in radians")]
        public void Test_Cos_inRads()
        {
            Assert.AreEqual(0.96017029, auto.Cos(6D), 0.001);
        }

        [TestCase(0, 0, ExpectedResult = 0)]
        [TestCase(10, 2, ExpectedResult = 12)]
        [TestCase(4, (-5), ExpectedResult = -1)]
        [TestCase(2, 11.9, ExpectedResult = 13.9)]
        public double Test_sum(double x, double y)
        {
            return auto.Add(x, y);
        }

        [Test]
        [Ignore("Ignored one"), Order(1)]
        [Repeat(1024)]
        public void Test_Tan_inRads()
        {
            Assert.That(auto.Tang(45D), Is.EqualTo(1.619D));
        }

        [Test, Order(3)]
        [Retry(5)]
        public void Test_SumArray_1()
        {
            Assert.That(auto.ArraySum(nums), Is.GreaterThan(131));
        }

        [Test, Order(2)]        
        public void Test_SumArray_2()
        {            
            Assert.That(auto.ArraySum(nums), Is.EqualTo(132));
        }

        [Test]
        public void Test_empty_pass()
        {
            Assert.Pass();
        }

        [Test]
        public void Test_string_DoesNotStartWith()
        {
            StringAssert.DoesNotStartWith("Is empty", NotEmptyText);
        }

        [Test]
        public void Test_string_DoesNotEndtWith()
        {
            StringAssert.DoesNotEndWith("empty note", NotEmptyText);
        }

        [Test]
        public void Test_string_IsEmpty()
        {
            Assert.That(EmptyText, Is.Empty); //"string EmptyText = string.Empty;" set in [OneTimeSeup] and "string EmptyText;" declared in 17th line
        }

        [Test]
        public void Test_empty_fail()
        {
            Assert.Fail();
        }
        

        [Test]
        public void Test_string_IsNotEmpty()
        {
            Assert.That(NotEmptyText, Is.Not.Empty);
        }

        [Test]
        public void Test_collection_IsEmpty()
        {
            Assert.That(new int[] { }, Is.Empty);
        }

        [Test]
        public void Test_String_Contains()
        {
            StringAssert.Contains("not", NotEmptyText);            
        }

        [Test]
        public void Test_String_StartWith()
        {
            StringAssert.StartsWith("Is not", NotEmptyText);
        }

        [Test]
        public void Test_String_EqualIgnCase()
        {
            StringAssert.AreEqualIgnoringCase("iS NoT emptY", NotEmptyText);
        }
                
        [Test]
        public void Test_IeeeReminder()
        {            
            Assert.That(auto.Remind(27, 4), Is.EqualTo(-1));

        }

        [Test]
        public void Test_DivideByZero()
        {
            Assert.Throws<DivideByZeroException>(new TestDelegate(() => auto.Div(13, 0)));
        }

        [Test]
        public void Test_MinArray()
        {
            Assert.That(auto.ArrayMin(nums), Is.EqualTo(-22));
        }

        [Test]
        public void Test_MaxArray()
        {
            Assert.That(auto.ArrayMax(nums), Is.EqualTo(63));
        }                
        
    }
}
